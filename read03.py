import math
import numpy as np
import astropy
from astropy import cosmology
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.coordinates import SkyCoord
cosmo = FlatLambdaCDM(H0=100. * u.km / u.s / u.Mpc, Om0=0.3)
from astropy.cosmology import Planck13, z_at_value
c = 299792.458
#----------------------------------------------------------------#
def sgxyz(sgl,sgb,cz):
    L = []
    
    p=180.0/math.pi
    L.append(cz*math.cos(sgb/p)*math.cos(sgl/p))
    L.append(cz*math.cos(sgb/p)*math.sin(sgl/p))    
    L.append(cz*math.sin(sgb/p))
    return L
#--------------------------------------------------------------------#
def getbin(val,minb,binsz):
    mod = math.modf((val - minb)/binsz)
    
    if mod[0] <= 0.5:
        x = int(mod[1])
    else:
        x = int(mod[1]+1)
    return x
#--------------------------------------------------------------------#
modelflag = 2
#----------------------------------------------------------------------#
##define input and output file names
#infile = "PantheonAllnewest.csv"   #supergalactic coordinates
#modfile = open("PantheonAllnewest_predpv_2M++.txt","w")
infile = "alfalfa_objects.csv"
modfile = open("alfalfa_predpv_2M++.txt", "w")

print ("Reading in input file      : ",infile)
print ("Reading out full sample to : ",modfile)
#---------------------------------------------------------------------------------------------------------#
twomcount = 0; gcount = 0
sgx_tm = []; sgy_tm = []; sgz_tm = []; vpred_pr = []
if(modelflag == 2):
    twomassfile = '2MPP_redshift.txt' #2M++ regular grid in redshift space - supergalactic
#---------------------------------------------------------------------------------------------------------#
vpred_x = []; vpred_y = []; vpred_z = []; vproj = []
for twomline in open(twomassfile,'rU'):
    if(twomline[0] != '#') :
        twomx = twomline.strip().split()

        sgx_tm.append(float(twomx[0]))
        sgy_tm.append(float(twomx[1]))
        sgz_tm.append(float(twomx[2]))
        vproj.append(float(twomx[3]))
        vpred_x.append(float(twomx[4]))
        vpred_y.append(float(twomx[5]))
        vpred_z.append(float(twomx[6]))
    twomcount += 1

print ("Number of predicted velocities (from density field):",twomcount,' ',twomassfile)
#----------------------------------------------------------------------------------------------------------#
if(modelflag == 2):
    #2M++ DETAILS
    dmin = -20000.
    dmax = 20000.
    nbins = 129

bsz = ((dmax-dmin)/float(nbins-1.))  #bin spacing of input grid
print ('minimum grid edge (dmin):',dmin,' size of bins (bsz): ',bsz)
#----------------------------------------------------------------------------------------------------------#
count = 0; galcount = 0; 
for line in open(infile,'rU'):
    if(line[0] == '#') :
        if(modelflag == 2):
            string1 = '#No, RA Dec pv[km/s]    comoving_distance[Mpc/h100]   zcmb zHubble W50\n'
            #string1 = '#No. RA DEC Type Velocity Redshift Redshift_Flag References Notes Photometry_Points Positions Redshift_Points Diameter_Points Associations Redshift-independent_Distances Classifications Images Spectra\n'
        modfile.write(string1)
    else:
        #read in RA,Dec and cz for each SN from input file
        x = line.strip().split(",")
        SN_flag = False
        W50 = float(x[3])
        if SN_flag == True:

            mastername = x[0] # ID
            mastersource = x[1] # source
            sgc = []
            ra0 = float(x[5])
            dec0 = float(x[6])
            ccc = SkyCoord(ra0, dec0, frame='icrs', unit='deg')
            xxx = ccc.transform_to('supergalactic')
            sgl = xxx.sgl.value
            sgb = xxx.sgb.value
            cz  = c*float(x[9])
            sgc = sgxyz(sgl,sgb,cz)
        else:
            mastername = x[0] # ID
            mastersource = ''
            sgc = []
            ra0 = float(x[1])
            dec0 = float(x[2])
            ccc = SkyCoord(ra0, dec0, frame='icrs', unit='deg')
            xxx = ccc.transform_to('supergalactic')
            sgl = xxx.sgl.value
            sgb = xxx.sgb.value
            cz  = c*float(x[5])
            sgc = sgxyz(sgl,sgb,cz)

        if dmin < sgc[0] < dmax and dmin < sgc[1] < dmax and dmin < sgc[2] < dmax:
            xbin = getbin(sgc[0],dmin,bsz)
            ybin = getbin(sgc[1],dmin,bsz)
            zbin = getbin(sgc[2],dmin,bsz)
            binindex = int(xbin)*nbins*nbins + int(ybin)*nbins + int(zbin)
            pv = vproj[binindex]
            xxx1 = (1. + cz/c)
            yyy2 = (1. + pv/c)
            yyy1 = xxx1/yyy2
            zzz1 = cosmo.comoving_distance(yyy1-1.)
            zzz2 = yyy1-1.
            zzz3 = cz/c
            v_x = vpred_x[binindex]
            v_y = vpred_y[binindex]
            v_z = vpred_z[binindex]
            if pv != -10000.:
                string1 = mastername+' '+str(ra0)+' '+str(dec0)+' % .4f % .8f %.8f % .8f'% (pv,zzz1.value,zzz3,zzz2)+' '+str(W50)+'\n'
                modfile.write(string1)
        count += 1
print ("Input Sample Size : ", count-1,"galaxies")
print ("Output Sample Size : ", galcount-1,"galaxies")
#----------------------------------------------------------------------------------------------------------------------#
modfile.close()

