from math import*
from numpy import*
import numpy as np
import csv

f0 = open("table1.txt","r")
f1 = open('HyperLeda_meandata_1638989735.txt')

logW = []
e_logW = []
incl = []
u_mag = []
g_mag = []
r_mag = []
i_mag = []
i_mag_corr = []
i_mag_corr_dust = []
z_mag = []
W = []
HVR = []
name = []
pgc = []




for line in f0:
	if line[307]!=' ':
		logW.append(float(line[54:59]))
		W.append(float(line[46:49]))
		HVR.append(float(line[40:45]))
		e_logW.append(float(line[60:65]))
		incl.append(float(line[34:36]))
		name.append(str(line[8:33]))
		pgc.append(int(line[0:7]))

		#u_mag.append(float(line[289:293]))
		#g_mag.append(float(line[295:299]))
		#r_mag.append(float(line[301:305]))
		i_mag_corr_dust.append(float(line[307:312]))
		i_mag_corr.append(float(line[193:198]))
		i_mag.append(float(line[97:102]))
		#z_mag.append(float(line[313:317]))

print(pgc)
pgc_full = []
name_full = []
ra_full = []
dec_full = []
#lol = csv.reader(f1, delimiter='\t')
#for l in lol:
#	if (l[0].startswith('#')!=True):
#		if int(l[0]) in pgc:
#			print(l[0])
#			pgc_full.append(int(l[0]))
#			name_full.append(str(l[1]))
#			ra_full.append(float(l[2]))
#			dec_full.append(float(l[3]))

#print(logW)
#np.save('cf4_data_names.npy',np.array([np.array(pgc), np.array(name),np.array(logW), np.array(incl), np.array(W), np.array(HVR), np.array(i_mag_corr_dust), np.array(i_mag_corr), np.array(i_mag),np.array(e_logW)]))
#np.save('hyperleda.npy',np.array([np.array(pgc_full), np.array(name_full), np.array(ra_full), np.array(dec_full)]))
#f0.close()

pgc = []
AGC = []
Name = []
Vhel = []
RAh = []
DEh = []
W_M50 = []
MHI = []
Inc = []
e_W50 = []

pgc_cf4 = np.load('cf4_data_names.npy')[0].astype(int)
#----------------------------------------------------------------------------------------------------#
vpred_x = []; vpred_y = []; vpred_z = []; vproj = []
for twomline in open("EDDtable09Dec2021002938.txt",'rU'):
    if(twomline[0] != '#') :
        twomx = twomline.strip().split(',')
        #print(twomx)
        if int(twomx[0]) in pgc_cf4:

        	pgc.append(int(twomx[0]))
        	AGC.append(float(twomx[1]))
        	Name.append(str(twomx[6]))
        	Vhel.append(float(twomx[11]))
        	W_M50.append(float(twomx[12]))
        	MHI.append(float(twomx[19]))
        	#Inc.append(float(twomx[27]))
        	RAh.append(float(twomx[7]))
        	DEh.append(float(twomx[8]))
        	e_W50.append(float(twomx[13]))

np.save('alfalfa_data.npy',np.array([np.array(pgc),np.array(W_M50), np.array(Vhel), np.array(MHI)]))

c = 299792.458 # km/s 
with open('model_velocity/alfalfa_objects.csv', 'w') as f:
    writer = csv.writer(f)
    header = ["#No.",'RA', 'DEC', 'W50', 'e_W50','z', 'pgc']
    writer.writerow(header)

    for i in range(len(pgc)):

        #h, m, s = RAJ2000[i].split(' ')
        #ra = 15*float(h)+15*float(m)/60+15*float(s)/3600
        #d, m, s = DEJ2000[i].split(' ')
        #dec = float(d)+float(m)/60+float(s)/3600
        ra = float(RAh[i])
        dec = float(DEh[i])
        zcmb = float(Vhel[i])/c
       
        if int(pgc[i]) in pgc_cf4:
            writer.writerow([i+1, ra, dec, float(W_M50[i]), float(e_W50[i]),zcmb, int(pgc[i])])
