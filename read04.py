from math import*
import numpy as np
import matplotlib.pyplot as plt
import astropy
from astropy import cosmology
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.coordinates import SkyCoord
cosmo = FlatLambdaCDM(H0=100 * u.km / u.s / u.Mpc, Om0=0.3)
#-------------------------------------------------------------------------------#
c = 299792.458
SNID=[];RA=[];Dec=[];zhel=[];zhelerr=[];zcmb=[];zcmberr=[];vpec=[];vpecerr=[];folder=[]
zcmb_m=[];vx=[];vy=[];vz=[]
xxx=[]
ux=[];uy=[];uz=[]
vvv=[]
dec01=[]
folder01=[]
#-------------------------------------------------------------------------------------#
f0 = open("PantheonAllnewest.csv","r")
f1 = open("expectation03_at_z_1.txt","r")
f2 = open("PantheonAllnewest_high_z.txt","w")
#--------------------------------------------------------------------------------------#
for line in f0:
    if line[0]!='#' and float(line.split(",")[9]) > 0.0665218237200617:
        SNID.append(str(line.split(",")[0]));folder01.append(str(line.split(",")[1]))
        RA.append(float(line.split(",")[5]));ra00 = float(line.split(",")[5])
        Dec.append(float(line.split(",")[6]));dec00 = float(line.split(",")[6])
        zcmb.append(float(line.split(",")[9]));z00 = float(line.split(",")[9])
        ccc = SkyCoord(ra00, dec00, frame='icrs', unit='deg')
        x001 = ccc.transform_to('galactic')
        distance00 = c*z00
        c00 = SkyCoord(l=x001.l.value*u.degree, b=x001.b.value*u.degree, distance=distance00*u.km/u.s, frame='galactic')
        c01 = c00.transform_to('supergalactic')
        c01.representation_type = 'cartesian'
        ux.append(c01.sgx.value);uy.append(c01.sgy.value);uz.append(c01.sgz.value)
print (len(RA))
#---------------------------------------------------------------------------------------------#
for line in f1:
    if line[0]!='#':
        zcmb_m.append(float(line.split()[0]))
        vx.append(float(line.split()[7]))
        vy.append(float(line.split()[8]))
        vz.append(float(line.split()[9]))
#---------------------------------------------------------------------------------------------#
k = np.searchsorted(zcmb_m, zcmb)
k1 = k.tolist()
zcmb_m01=[];vx01=[];vy01=[];vz01=[]
for j in k1:
    zcmb_m01.append(zcmb_m[j])
    vx01.append(vx[j])
    vy01.append(vy[j])
    vz01.append(vz[j])
#----------------------------------------------------------------------------------------------#
f2.write('#SNID IAUC RA_host Dec_host pv[km/s]    comoving_distance[Mpc/h100]   zcmb zHubble')
f2.write('\n')
#----------------------------------------------------------------------------------------------#
for i in range(len(SNID)):
    vpred = ((ux[i]*vx01[i])+(uy[i]*vy01[i])+(uz[i]*vz01[i]))/(np.sqrt(ux[i]**2+uy[i]**2+uz[i]**2))
    xxx1 = (1. + zcmb[i])
    yyy2 = (1. + vpred/c)
    yyy1 = xxx1/yyy2
    zzz1 = cosmo.comoving_distance(yyy1-1.)
    zzz2 = yyy1-1.
    zzz3 = zcmb[i]
    f2.write("%s %s %f %.4f %.4f %.8f %.8f %.8f %.8f\n" % (SNID[i],folder01[i],RA[i],Dec[i],vpred,250.0,zzz1.value,zzz3,zzz2))
    print (zcmb[i],zcmb_m01[i])
    vvv.append(vpred)
    xxx.append(zcmb[i])
    dec01.append(Dec[i])
print (len(xxx),len(zcmb))
#--------------------------------------------------------------------------------------------#
plt.scatter(xxx,vvv,c=dec01,s=2)#,color='black'
plt.xlabel(r'zcmb')
plt.ylabel(r'$PV$ [km/s]')
#plt.legend()
plt.grid(b=True, which='both', color='0.65',linestyle=':')
plt.tight_layout()
plt.xscale('log')
plt.savefig('z_pv.png')
plt.savefig('z_pv.pdf')
plt.show()
#---------------------------------------------------------------------------------------------#
f0.close();f1.close();f2.close()
